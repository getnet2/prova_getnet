package com.execute_test;

import static io.restassured.RestAssured.basePath;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.when;
import static org.hamcrest.core.IsEqual.equalTo;

import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;

import com.test.SingleUser;

import io.restassured.RestAssured;

public class Execute_Test {
	String singleUserApi = "/2";
	String singleUserNotFoundApi = "/23";
	
	@Before
	public void beforeScenario() {
		RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
		baseURI = "https://reqres.in/";
		basePath = "/api/users";
	}

	@Test
	public void singleUserFound() {
		SingleUser singleUser = new SingleUser();
		when().
			get(singleUserApi).
		then().
			statusCode(HttpStatus.SC_OK).
			body(singleUser.getCampoId(),equalTo(singleUser.getValorId()),
				 singleUser.getCampoEmail(), equalTo(singleUser.getValorEmail()),
				 singleUser.getCampoFirstName(), equalTo(singleUser.getValorFirstName()),
				 singleUser.getCampoLastName(), equalTo(singleUser.getValorLastName())).
			log().all();
	}
	
	@Test
	public void singleUserNotFound() {
	when().
		get(singleUserNotFoundApi).
	then().
		statusCode(HttpStatus.SC_NOT_FOUND);
	}
}