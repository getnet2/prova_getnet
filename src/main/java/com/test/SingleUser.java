package com.test;

public class SingleUser {
	private String campoId = "data.id";
	private String campoEmail = "data.email";
	private String campoFirstName = "data.first_name";
	private String campoLastName = "data.last_name";
	private int valorId = 2;
	private String valorEmail = "janet.weaver@reqres.in";
	private String valorFirstName = "Janet";
	private String valorLastName = "Weaver";

	public String getCampoId() {
		return campoId;
	}

	public String getCampoEmail() {
		return campoEmail;
	}

	public String getCampoFirstName() {
		return campoFirstName;
	}

	public String getCampoLastName() {
		return campoLastName;
	}

	public int getValorId() {
		return valorId;
	}

	public String getValorEmail() {
		return valorEmail;
	}

	public String getValorFirstName() {
		return valorFirstName;
	}

	public String getValorLastName() {
		return valorLastName;
	}

}
